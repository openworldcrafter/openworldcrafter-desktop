#!/bin/bash

# Installs required build tools on Ubuntu (tested on 17.10)
# Must be run with sudo powers

# Update APT database
apt update

# Install cURL. For some reason Ubuntu doesn't have it preinstalled.
apt install curl

# Install nodesource repository
curl -sSL https://deb.nodesource.com/setup_8.x | bash -

# Install yarn repository
curl -sSL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list

# Install WINE repository (WINE is needed to build Windows packages on Linux)
# Following steps given at https://askubuntu.com/questions/316025/how-to-install-and-configure-wine#316029
releasekey=$(mktemp)
curl -sSL https://dl.winehq.org/wine-builds/Release.key > $releasekey
apt-key add $releasekey
apt-add-repository https://dl.winehq.org/wine-builds/ubuntu/

apt update

# Install needed packages
apt install -y snapd rpm libasound2-plugins:i386 wine-stable-i386 wine-stable winehq-stable nodejs yarn
# Install snapcraft build tools from snapcraft
snap install snapcraft --classic

