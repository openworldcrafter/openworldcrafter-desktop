#!/bin/bash

# Start with a clean slate
rm -rf cleanroom
mkdir cleanroom
rm -rf ../openworldcrafter/dist/desktop

# Go to the core repo and run our desktop build
cd ../openworldcrafter

# check for uncommitted changes
if [[ -n $(git status -s) ]]; then
    echo "There are uncomitted changes! Please commit or stash them before building for production. All production builds must be compiled from committed code."
else
    commit=$(git rev-parse HEAD)
    echo "Compiling desktop from build $commit"

    OWC_ORIGIN="https://online.openworldcrafter.com" NODE_ENV=production OWC_COMMIT_ID=$(git rev-parse HEAD) webpack-cli --config webpack/desktop.webpack.config.js -p

    cd ../openworldcrafter-desktop

    # Copy native.js, placeholder.js, and entry.js into the cleanroom
    mkdir cleanroom/js
    cp ../openworldcrafter/js/native.js cleanroom/js/native.js
    cp ../openworldcrafter/js/placeholder.js cleanroom/js/placeholder.js
    cp ../openworldcrafter/js/entry.js cleanroom/js/entry.js

    # Copy over the build
    cp -r ../openworldcrafter/dist/desktop/* cleanroom

    # Copy the package.json and yarn.lock files, then install them
    cp ../openworldcrafter/package.json ../openworldcrafter/yarn.lock cleanroom
    cd cleanroom
    NODE_ENV=production yarn
    cd ..

    # Create the build. For technical reasons, only x64 binaries are created for Linux.
    electron-builder --x64 --win nsis --mac tar.gz --linux rpm deb snap --project cleanroom

    # Remove the blockmap file. I don't know what it is and I don't seem to need
    # it.
    rm cleanroom/dist/openworldcrafter*.exe.blockmap

    # Move the output out of the cleanroom directory
    rm -rf dist/$commit
    mkdir dist
    mkdir dist/$commit
    mv cleanroom/dist/openworldcrafter* dist/$commit

    # Calculate SHA/MD5 hashes and PGP signatures, if asked to do so
    if [[ "$1" = "--sign" ]]; then
        cd cleanroom/dist

        mkdir ../verification

        shasum -a 256 openworldcrafter* > ../verification/checksums-sha256.txt
        md5sum openworldcrafter* > ../verification/checksums-md5.txt

        for file in openworldcrafter* ; do
            gpg2 --detach-sign --armor -o "../verification/$file.sig" "$file"
        done

        cd ../../
    fi
fi
